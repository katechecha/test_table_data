// данные для форматирования текста
const numberTranslitions = {
    vritypeId: {
        1: 'первичная',
        2: 'периодическая'
    },
    applicabilityText: [ 'СИ не пригодно', 'СИ пригодно' ],
    certnoticeText: ['Свидетельство', 'Извещение'],
    briefText: {
        null: 'не указано',
        0: 'нет',
        1: 'да'
    }
}

// форматирование даты
function date(element) {
    return element.replaceAll('-','.')
}

// форматирование текста
function ParentFieldsRename(element) {
    element.vritype_id = numberTranslitions.vritypeId[element.vritype_id]
    element.applicability = numberTranslitions.applicabilityText[element.applicability]
    if (element.protocol == 0) {
        element.protocol = 'нет протокола'
    } else {
        element.protocol = 'есть протокол'
    }
    element.verification_at = date(element.verification_at)
}

// функции форматирования данных с бэка по тз
export const func = {
    TableFieldsRename: (element) => {
        element.certnotice = numberTranslitions.certnoticeText[element.applicability]
        ParentFieldsRename(element)
    },
    DocumentFieldsRename: (element) => {
        ParentFieldsRename(element)
        if (element.reg_number === null) {
            element.reg_number = 'нет рег. номера'
        }
        element.brief = numberTranslitions.briefText[element.brief]
        element.valid_until = date(element.valid_until)
    }
 }