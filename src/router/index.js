import { createRouter, createWebHistory } from 'vue-router'
import TableView from '../views/TableView.vue'
import TableItem from '../views/TableItem.vue'

const routes = [
  {
    path: '/',
    name: 'Таблица',
    component: TableView
  },
  {
    path: '/results/:id',
    name: 'Документ',
    component: TableItem
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
